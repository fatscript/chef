# CHEF 🧑‍🍳

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

`chef` is an open-source package manager designed specifically for [FatScript](https://fatscript.org) projects.

## Getting Started

### Prerequisites

To use `chef`, ensure you have `fry` (^3.4.0), the [FatScript](https://fatscript.org) interpreter, and `git` installed on your system.

### Installation

Clone the `chef` repository and install it locally:

```bash
git clone https://gitlab.com/fatscript/chef.git
cd chef
fry -b $HOME/.local/bin/chef chef.fat
```

This will build `chef` using `fry` and place the executable in `$HOME/.local/bin`, which should be in your PATH for easy execution.

## Usage

After installing `chef`, initialize the menu for your FatScript project. Replace `your-project-folder` with the path to your project directory:

```bash
cd your-project-folder
chef menuadd fatscript.org
```

To manage your project dependencies, use the following commands:

```bash
# Add a dependency, optionally with version
chef include library-name:version-tag

# Remove a dependency
chef exclude library-name

# Update and synchronize dependencies; use 'fresh' to discard the current lock file
chef restock
```

Dependencies are listed in the `ingredients.txt` file in your project root, which `chef` automatically manages. Here’s an example of what `ingredients.txt` might look like:

```
library-one:1.0.0
library-two:2.5.3
library-three
```

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [chef GitLab](https://gitlab.com/fatscript/chef/issues) page.

### Donations

If you find `chef` useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## License

[MIT License](LICENSE) © 2024 Antonio Prates.
